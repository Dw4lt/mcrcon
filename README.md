# MCRcon

This project is based on [Adrian-Turjak](https://github.com/Uncaught-Exceptions/MCRcon)'s MCRcon fork.

- [Rcon protocol](http://wiki.vg/Rcon)

### Installation
---

The library can be installed with pip:

    pip3 install -U git+https://gitlab.com/Dw4lt/mcrcon.git#master

### Usage
---

It is recommended to use both the client and server istances inside a `with` statement to avoid dealing with connection states manually.

**Client example:**
```py
from mcrcon import GenericRconClient

with GenericRconClient(ip="localhost", port=25585, password="my_password") as client:
    rid = client.send(message="ping")
    print(client.await_response(rid, timeout=1)).
```

**Server example:**
```py
from mcrcon import RconServer, Message, MessageTypes

with RconServer(ip="localhost", port=25585, password="my_password") as server:
    while True:
        request = server.get_incoming_request(timeout_in_seconds=None)

        match request.message.message_as_string():
            case "stop":
                break
            case "ping":
                request.reply(Message(id=request.message.id, message_type=MessageTypes.SERVERDATA_RESPONSE_VALUE, message="pong"))
            case default:
                print("Unknown command")
```



Note: instantiating `GenericRconClient` doesn't automatically establish a connection, instead it will be established as needed, the object can exist indefinitely.
