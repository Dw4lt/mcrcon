from queue import Empty
import pytest
from mcrcon import Message, MessageTypes, GenericRconClient, RconInvalidCredentials, RconServer, Request, RconException
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    clnt: GenericRconClient
    srv: RconServer

password = 'password'
ip = '127.0.0.1'
port = 9999
payload = 'test'


@pytest.fixture
def client():
    clnt = GenericRconClient(ip=ip, port=port, password=password)
    yield clnt
    clnt.disconnect()
    del clnt
    clnt = None

second_client = client


@pytest.fixture
def server():
    srv = RconServer(ip=ip, port=port, password=password)
    with srv:
        yield srv
    srv.stop()


def test_login(client: GenericRconClient, server: RconServer):
    with client, server:
        id = client.send(message_type=MessageTypes.SERVERDATA_EXECCOMMAND, message=payload)
        try:
            request = server.get_incoming_request(timeout_in_seconds=3)
            assert request
            assert request.message.id == id
            assert request.message.message_as_string() == payload
        except Empty:
            assert False, 'No incoming request detected.'

def test_timeout(client: GenericRconClient, server: RconServer):
    with client, server:
        # Test server timeout
        with pytest.raises(Empty):
            server.get_incoming_request(timeout_in_seconds=0.01)

        # Test client timeout
        id = client.send(message="irrelevant", message_type=MessageTypes.SERVERDATA_EXECCOMMAND)
        with pytest.raises(RconException):
            client.await_response(id, timeout=0.01)


def test_restart(client: GenericRconClient, server: RconServer):
    """Regression test: Test restart of client to ensure threads are only started once."""
    with server:
        for _ in range(0, 5):
            with client:
                pass


def test_multiple_connections(client: GenericRconClient, second_client: GenericRconClient, server: RconServer):
    with client, second_client, server:
        id1 = client.send(message_type=MessageTypes.SERVERDATA_EXECCOMMAND, message='client1')
        id2 = second_client.send(message_type=MessageTypes.SERVERDATA_EXECCOMMAND, message='client2')
        try:
            requests: list[Request] = [server.get_incoming_request(timeout_in_seconds=3) for i in range(2)]
            requests.sort(key=lambda req: req.message.message_as_string())

            requests[1].reply(Message(requests[1].message.id, message_type=MessageTypes.SERVERDATA_RESPONSE_VALUE, message='success 2'))
            requests[0].reply(Message(requests[0].message.id, message_type=MessageTypes.SERVERDATA_RESPONSE_VALUE, message='success 1'))

            assert client.await_response(id1, timeout=3) == 'success 1'
            assert second_client.await_response(id2, timeout=3) == 'success 2'
        except Empty:
            assert False, 'No incoming request detected.'


def test_invalid_password(server: RconServer):
    client = GenericRconClient(ip=ip, port=port, password='wrong_password')
    try:
        client.connect()
    except RconInvalidCredentials:
        return
    except:
        raise
    raise Exception('Login should not have worked.')


def test_message_splitting(server: RconServer, client: GenericRconClient):
    total_size = 10_000
    with client, server:
        client._send(message_type=MessageTypes.SERVERDATA_EXECCOMMAND, message='irrelevant')
        request = server.get_incoming_request(timeout_in_seconds=3)
        request.reply(Message(request.message.id, MessageTypes.SERVERDATA_RESPONSE_VALUE, b'a' * total_size))
        response = client.await_response(request.message.id)
        assert len(response) == total_size
