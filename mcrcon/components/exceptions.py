class RconException(Exception):
    def __init__(self, *args) -> None:
        super().__init__(*args)


class RconInvalidCredentials(RconException):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)


class IncompletePacket(RconException):
    def __init__(self, minimum):
        self.minimum = minimum


def rcon_exception_wrapper(f):
    def wrapper(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except RconException:
            raise
        except Exception as e:
            import sys
            _, _, traceback = sys.exc_info()
            raise RconException(e).with_traceback(traceback) from None
    return wrapper
