from __future__ import annotations
from abc import abstractmethod

import threading
from dataclasses import dataclass
from queue import Empty, Queue
from socket import socket as Socket
from threading import Thread
from socketserver import TCPServer, ThreadingMixIn, BaseRequestHandler
from threading import Event

import bcrypt

from . import Connection, Loggable, Message, MessageTypes, RconException, rcon_exception_wrapper


def hash_password(password: str | bytes, salt: bytes) -> bytes:
    password = password if isinstance(password, bytes) else bytes(password, Message.ENCODING)
    return bcrypt.hashpw(password=password, salt=salt)


@dataclass
class Request:
    message: Message
    _reply_queue: Queue[Message]

    def __repr__(self) -> str:
        return f'{self.__class__.__name__}({self.message=}, {self._reply_queue=})'

    def reply(self, message: Message):
        self._reply_queue.put(message)


class ThreadedTCPServer(ThreadingMixIn, TCPServer, Loggable):
    def __init__(self, ip: str, port: int, salt: bytes, hashed_password: bytes, timeout: float | None = None) -> None:
        self.name = f'{ip}:{port}'

        self._incoming_requests_queue: Queue[Request] = Queue()

        self._salt: bytes = salt
        self._hashed_password: bytes = hashed_password
        self._main_thread: Thread | None = None
        self.allow_reuse_address = True
        self._poll_interval = 0.2
        self.shutdown_event = Event()
        TCPServer.__init__(self, (ip, port), RequestHandler)

    @abstractmethod
    def __enter__(self):
        pass

    @abstractmethod
    def __exit__(self, *_):
        pass

    @abstractmethod
    def start(self):
        pass

    @abstractmethod
    def stop(self):
        pass

    def server_bind(self) -> None:
        self.allow_reuse_address = True
        super().server_bind()
        self.log.debug(f'Rcon server listening on {self.server_address[0]}:{self.server_address[1]}')

    def shutdown(self) -> None:
        self.shutdown_event.set()
        TCPServer.shutdown(self)
        ThreadingMixIn.server_close(self)
        if self._main_thread is not None:
            del self._main_thread
            self._main_thread = None

    def _main(self):
        TCPServer.serve_forever(self, self._poll_interval)

    def start_serving(self, poll_interval: float = 0.2) -> None:
        '''Non-blocking start.'''
        if self._main_thread is not None:
            raise RconException('Attempting to start already running server. Aborting.')
        # Set flag
        self._poll_interval = poll_interval
        self._main_thread = threading.Thread(target=self._main)
        self._main_thread.start()


class RequestHandler(BaseRequestHandler, Loggable):
    def setup(self) -> None:
        self.request: Socket
        self.server: ThreadedTCPServer
        super().setup()
        self.request.settimeout(0.1)
        self._connection: Connection = Connection(self.request)
        self._outgoing_replies: Queue[Message] = Queue()

    def handle(self):
        while not self.server.shutdown_event.is_set():
            try:
                try:
                    while True:
                        message_to_send = self._outgoing_replies.get_nowait()
                        self.reply(message_to_send)
                except Empty:
                    pass

                message = self._connection.get_message(0.1)
                if message is None:
                    continue

                if not self._connection.is_authenticated():
                    if message.message_type == MessageTypes.SERVERDATA_AUTH:
                        self.__handle_login(message)
                    else:
                        self.log.debug(f'Unauthorised request: {message}')
                        return
                else:
                    if message.message_type == MessageTypes.SERVERDATA_RESPONSE_VALUE:
                        self.__on_command_done_request(message)
                    elif message.message_type == MessageTypes.SERVERDATA_EXECCOMMAND:
                        self.__add_command(message)
                    else:
                        self.log.debug(f'Invalid request type {message.message_type}')
            except Exception as e:
                import traceback

                traceback.print_exception(e)
                pass

    def finish(self) -> None:
        self.log.debug(f'Terminating handler.')
        self._connection.disconnect()
        return super().finish()

    def __handle_login(self, message: Message):
        if hash_password(message.message, self.server._salt) == self.server._hashed_password:
            self._connection.declare_authorized()
            self.reply(Message(message.id, MessageTypes.SERVERDATA_AUTH_RESPONSE, b'Welcome!'))
        else:
            self.__warn_invalid_session()

    def __on_command_done_request(self, message: Message):
        self.reply(Message(message.id, MessageTypes.SERVERDATA_RESPONSE_VALUE, b''))
        self.reply(Message(message.id, MessageTypes.SERVERDATA_RESPONSE_VALUE, b'\x00\x00\x00\x01\x00\x00\x00\x00'))
        self.log.debug(f'Sent "end of fragmented message" sentinel')

    def __add_command(self, message: Message):
        self.log.debug(f'Received request from user. Passing it on to main thread.')
        req = Request(message=message, _reply_queue=self._outgoing_replies)
        self.server._incoming_requests_queue.put(req)

    def __warn_invalid_session(self):
        self.reply(Message(-1, MessageTypes.SERVERDATA_AUTH_RESPONSE, b'Invalid session.'))
        self.log.debug(f'Invalid login.')

    def reply(self, message: Message):
        '''
        ### Blocking
        Send a message to a client.
        '''
        self.log.debug(f'About to reply with "{message}"')
        for msg in message.split_into_chunks_if_necessary():
            self._connection.raw_send(msg.package())
        self.log.debug(f'Replied with "{message}"')


class RconServer(ThreadedTCPServer, Loggable):
    @rcon_exception_wrapper
    def __init__(self, *, password: str | bytes, ip: str = '127.0.0.1', port: int = 25585, timeout: float | None = 10):
        # password stuff
        salt: bytes = bcrypt.gensalt()
        hashed_password: bytes = hash_password(password, salt)
        del password

        ThreadedTCPServer.__init__(self, ip=ip, port=port, salt=salt, hashed_password=hashed_password, timeout=timeout)

    def __enter__(self) -> RconServer:
        self.start()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.stop()

    def start(self):
        '''Start listening for incoming requests in a new daemon thread (non-blocking).'''
        if self._main_thread is not None:
            self.log.warning('Attempting to start already running server. Ignoring.')
            return
        self.start_serving()

    def stop(self):
        '''Stop the rcon server.'''
        self.shutdown()
        self.log.info('Server fully stopped.')

    def get_incoming_request(self, timeout_in_seconds: float | None = None) -> Request:
        '''Wait for a new request to arrive (optional blocking).

        A `queue.Empty` exception may be raised if timeout is reached. A timeout of `None` blocks indefinitely.

        Request must be dealt with externally.

        #### Try to send back a reply soon, as the client is likely waiting.
        '''
        return self._incoming_requests_queue.get(block=True, timeout=timeout_in_seconds)
