import struct
from dataclasses import dataclass
from threading import Event, RLock, Thread
from typing import Iterable
from socket import socket as Socket, SHUT_RDWR
import select
from queue import Queue, Empty

from .exceptions import RconException, rcon_exception_wrapper
from .loggable import Loggable


@dataclass(frozen=True)
class MessageTypes:
    SERVERDATA_AUTH: int = 3
    SERVERDATA_EXECCOMMAND: int = 2
    SERVERDATA_AUTH_RESPONSE: int = 2
    SERVERDATA_RESPONSE_VALUE: int = 0


class Message(Loggable):
    HEADER_SIZE = 12
    TRAILER_SIZE = 2
    MAX_PAYLOAD_SIZE = 4096
    ENCODING = 'ascii'

    def __init__(self, id: int, message_type: int, message: bytes | str, advertised_size: int = 0) -> None:
        super().__init__()
        self.id: int = id
        self.message_type: int = message_type
        self.message: bytes = message if isinstance(message, bytes) else message.encode(Message.ENCODING)
        self.advertised_size: int = advertised_size

    def __post_init__(self):
        super().__init__()

    def __iadd__(self, other: 'Message') -> 'Message':
        if not other.__class__ == self.__class__:
            raise NotImplementedError('Messages can only be added to other messages.')
        assert self.id == other.id and self.message_type == other.message_type
        return Message(
            id=self.id,
            message_type=self.message_type,
            message=self.message + other.message,
            advertised_size=self.advertised_size + other.advertised_size,
        )

    def __repr__(self) -> str:
        msg = self.message_as_string() if not self.message_type == MessageTypes.SERVERDATA_AUTH else '<REDACTED>'
        return f'{self.__class__.__name__}({self.id=}, {self.message_type=}, message="{msg}")'

    def split_into_chunks_if_necessary(self) -> Iterable["Message"]:
        message = self.message
        if len(message) > Message.MAX_PAYLOAD_SIZE:
            while len(message) > 0:
                chunk = message[: Message.MAX_PAYLOAD_SIZE]
                msg = Message(
                    id=self.id,
                    message_type=self.message_type,
                    message=chunk,
                    advertised_size=len(chunk) + Message.HEADER_SIZE + Message.TRAILER_SIZE - 4,
                )
                message = message[Message.MAX_PAYLOAD_SIZE :]
                yield msg
        else:
            yield self

    def package(self) -> bytes:
        lenght = Message.HEADER_SIZE - 4 + len(self.message) + Message.TRAILER_SIZE
        return struct.pack("<iii", lenght, self.id, self.message_type) + self.message + b"\x00\x00"

    def size_in_buffer(self) -> int:
        return Message.HEADER_SIZE + len(self.message) + Message.TRAILER_SIZE

    def message_as_string(self) -> str:
        return self.message.decode(Message.ENCODING)

    @staticmethod
    def from_bytes(data: bytes) -> 'Message':
        """Note: It's possible that not the entire buffer is read."""
        advertised_size, id, message_type = struct.unpack("<iii", data[: Message.HEADER_SIZE])
        actual_size = min(
            advertised_size + 4,
            Message.MAX_PAYLOAD_SIZE + Message.HEADER_SIZE + Message.TRAILER_SIZE,
        )
        payload, padding = (
            data[Message.HEADER_SIZE : actual_size - Message.TRAILER_SIZE],
            data[actual_size - 2 : actual_size],
        )
        if not padding == b"\x00\x00":
            raise RconException('Malformed packet, does not end with \\x00\\x00.')
        if not advertised_size == len(payload) + Message.HEADER_SIZE + Message.TRAILER_SIZE - 4:
            raise RconException('Malformed packet, advertised size does not match payload size')
        return Message(id=id, message_type=message_type, message=payload, advertised_size=advertised_size)


class Connection(Loggable):
    def __init__(self, socket: Socket = None, already_authenticated: bool = False):  # type: ignore
        super().__init__()
        self.__lock = RLock()
        self.__socket: Socket = socket
        self.__authenticated: bool = already_authenticated

        self.__incoming_message_queue: Queue[Message] = Queue()
        self._shutdown_event: Event = Event()
        self.__polling_thread: Thread = self.__create_polling_thread()

        if self.__socket is not None:
            self.__polling_thread.start()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, *_):
        if exc_type is not None:
            self.disconnect()

    def __repr__(self) -> str:
        return f'{self.__class__.__name__}({self.__socket=}, {self.__authenticated=})'

    def __create_polling_thread(self) -> Thread:
        return Thread(daemon=True, target=self.__read_loop)

    def __read_loop(self):
        while not self._shutdown_event.is_set():
            readable, _, _ = select.select([self.__socket], [], [], 0.3)  # Effectively "select.poll()", but also works on windows...
            if readable:
                try:
                    with self.__lock:
                        if not self._shutdown_event.is_set():  # Double check due to lock
                            data = self.__socket.recv(4)  # Size
                            if data:
                                (size,) = struct.unpack("<i", data)
                                size_without_overhead = size - Message.HEADER_SIZE - Message.TRAILER_SIZE + 4
                                if size_without_overhead > Message.MAX_PAYLOAD_SIZE:
                                    raise RconException(
                                        f'Size exceeds maximum. {size_without_overhead}>{Message.MAX_PAYLOAD_SIZE}'
                                    )
                                data += self.__socket.recv(size)
                                message = Message.from_bytes(data)
                                self.log.debug(f'Received {message}')
                                self.__incoming_message_queue.put(message)
                            else:
                                break
                except BlockingIOError:
                    # Socket is not ready for recv, so we'll wait for a bit and try again
                    select.select([self.__socket], [], [], 0.01)

    def disconnect(self) -> None:
        with self.__lock:
            self._shutdown_event.set()
            if self.is_session_valid():
                try:
                    self.__socket.shutdown(SHUT_RDWR)
                except OSError:
                    pass
                finally:
                    self.__socket.close()
                self.__socket = None  # type: ignore
            self.__authenticated = False

    @rcon_exception_wrapper
    def set_socket(self, socket: Socket):
        with self.__lock:
            if self.__socket is not None:
                self.disconnect()
        if self.__polling_thread.is_alive():
            self.__polling_thread.join(1)
        with self.__lock:
            self._shutdown_event.clear()
            self.__socket = socket
            self.__polling_thread = self.__create_polling_thread()
            self.__polling_thread.start()

    def is_session_valid(self) -> bool:
        with self.__lock:
            return not self.is_closed() and self.__authenticated

    def is_closed(self) -> bool:
        with self.__lock:
            return self.__socket is None or self.__socket.fileno() == -1

    def is_authenticated(self) -> bool:
        with self.__lock:
            return self.__authenticated

    def declare_authorized(self) -> None:
        with self.__lock:
            self.__authenticated = True
            self.log.debug(f'Authorization granted on {id(self)}. Client is logged in.')

    @rcon_exception_wrapper
    def raw_send(self, data: bytes):
        with self.__lock:
            if self.is_closed():
                raise RconException('Attempted writing to closed socket.')
            self.__socket.send(data)

    def get_message(self, timeout: float | None = None) -> Message | None:
        try:
            return self.__incoming_message_queue.get(block=(timeout != 0), timeout=timeout)
        except Empty:
            return None
