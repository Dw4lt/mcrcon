import random
import socket
from socket import socket as Socket
import time


from . import Connection, Message, MessageTypes, RconException, RconInvalidCredentials, Loggable, rcon_exception_wrapper


class RconClientProtocol(Loggable):
    MAGIC_MESSAGE_ID = 101

    def __init__(self, *, ip: str, password: str, port: int, timeout: float | None):
        super().__init__()
        self._timeout = timeout

        self.ip = ip
        self.port = port
        self.password = password

        self.__connection: Connection = Connection()

    def is_session_valid(self):
        return self.__connection.is_session_valid()

    def connect(self):
        self._ensure_connected()

    def __enter__(self):
        self._ensure_connected()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.disconnect()

    def disconnect(self):
        if not self.__connection.is_closed():
            self.__connection.disconnect()

    @rcon_exception_wrapper
    def _send(self, message_type: int, message: str | bytes) -> int:
        '''Send an RCON message to the server. Requires the connection to be already established.

        Does not wait for a response, instead returns the ID that can be used to await it manually.'''
        if self.__connection.is_closed():
            raise RconException("Must connect before sending data")

        # Send a request packet
        id = self.__gen_id()
        msg = Message(id=id, message_type=message_type, message=message)
        self.__connection.raw_send(msg.package())
        return id

    @rcon_exception_wrapper
    def await_response(self, expected_id: int, timeout: float | None = 0) -> str:
        if timeout == 0:
            timeout = self._timeout  # Hacky way to allow defaulting to `self._timeout`
        self.log.debug(f'Awaiting response for message {expected_id}')
        response = []
        start = time.time()
        while True:
            message = self.__connection.get_message(timeout=timeout)
            if message is None:
                raise RconException('No response received.')
            if message.message_type == MessageTypes.SERVERDATA_AUTH_RESPONSE and message.id == -1:
                raise RconInvalidCredentials('Login failed')
            elif message.message_type == MessageTypes.SERVERDATA_AUTH_RESPONSE or (
                message.message_type == MessageTypes.SERVERDATA_RESPONSE_VALUE and len(message.message) == 0
            ):
                self.__connection.declare_authorized()
                break
            elif expected_id == message.id:
                response.append(message.message_as_string())
                length = len(message.message)
                if length >= Message.MAX_PAYLOAD_SIZE:
                    self.__request_response()
                    start = time.time()
                else:
                    break
            elif message.id == RconClientProtocol.MAGIC_MESSAGE_ID:
                self.log.debug('Magic packet received, end of transmission.')
                break
            elif expected_id != 0 and id != expected_id:
                raise RconException(
                    f"{message.id} {message.message_type} does not match {expected_id}. Different response detected!"
                )
            if timeout is not None and time.time() - start >= timeout:
                break
        return ''.join(response)

    @rcon_exception_wrapper
    def _ensure_connected(self):
        if self.__connection.is_closed():
            self.log.debug('Connection was closed. Attempting to establish.')
            self.__connection.set_socket(self.__connect_to_server_socket())
        if not self.__connection.is_authenticated():
            self.log.debug('Not yet authenticated. Authenticating now.')
            self.__login()

    @rcon_exception_wrapper
    def __connect_to_server_socket(self) -> Socket:
        sc = Socket(socket.AF_INET, socket.SOCK_STREAM)
        sc.connect((self.ip, self.port))
        sc.settimeout(0.2)
        self.log.debug('Connection established')
        return sc

    def __login(self):
        self.log.info('Logging in')
        id = self._send(MessageTypes.SERVERDATA_AUTH, self.password)
        return self.await_response(id)

    def __request_response(self):
        """Intentionally cause error to detect end of actual package"""
        self.__connection.raw_send(
            Message(
                id=RconClientProtocol.MAGIC_MESSAGE_ID, message_type=MessageTypes.SERVERDATA_RESPONSE_VALUE, message=b'testin'
            ).package()
        )

    def __gen_id(self) -> int:
        '''Generate a random ID. Any magic packet numbers must stay below 200 to avoid collisions.'''
        return random.randint(200, 2**31 - 1)
