import logging
from logging import LoggerAdapter


class Loggable:
    @property
    def log(self) -> logging.Logger | LoggerAdapter:
        if getattr(self, '_log', None) is None:
            self._log: logging.Logger | LoggerAdapter = logging.getLogger(self.__class__.__name__)
            self._log = LoggerAdapter(self._log, {'library': 'mcrcon'})
        return self._log
