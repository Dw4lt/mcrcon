import logging

from . import MinecraftRconClient, Message, MessageTypes, RconServer


logger=logging.getLogger(__name__)


def tests():
    logger.info("Entering Test Mode")

    import configparser

    config = configparser.RawConfigParser()
    config.read('../config.ini')
    server_ip = config.get('rcon', 'server_ip')
    rcon_port = config.getint('rcon', 'rcon_port')
    rcon_key = config.get('rcon', 'rcon_key')

    server = RconServer(ip=server_ip, password=rcon_key, port=rcon_port)
    client = MinecraftRconClient(ip=server_ip, port=rcon_port, password=rcon_key)
    with server, client:
        for i in range(0, 100):
            # Client send
            sent_id = client.send(f'Hello server! {i=}')  # Do not block, as we need to send a reply through the server instance first.

            # Server handle
            req = server.get_incoming_request(3)
            req_id = req.message.id
            req.reply(Message(req_id, MessageTypes.SERVERDATA_RESPONSE_VALUE, f'Hello client! {i=}'))

            # Client await response
            client.await_response(sent_id)


def main():
    import sys
    FORMAT = '[%(levelname)s] {%(filename)s:%(lineno)d} <%(name)s>: %(message)s'
    file, stream = logging.FileHandler('rcon.log'), logging.StreamHandler(sys.stdout)
    logging.basicConfig(
        handlers=[file, stream], format=FORMAT, datefmt='%Y-%m-%d %H:%M:%S', level=logging.DEBUG
    )
    tests()

if __name__ == "__main__":
    main()

