import re
from .generic import GenericRconClient


class MinecraftRconClient(GenericRconClient):
    def __init__(self, *, ip: str, password: str, port: int = 25575, timeout: float | None = None):
        super().__init__(ip=ip, password=password, port=port, timeout=timeout)

    def _get_log_prefix_regex(self) -> str:
        return r'^(\[[^\[\]]*\] ?)*: '

    def ban(self, player: str):
        self.send(f'ban {player}')

    def get_name(self, string: str) -> str | None:
        a: re.Match[str] = re.search(r'(?<=<)[a-zA-Z0-9_ ]*(?=>)', string) # type: ignore
        if a:
            b: str = a.group(0)
            b = b.replace(" ", "_")
            self.log.info(f"name ({string}) = {b}")
            return b
        return None

    def unban(self, player: str) -> str | None:
        return self.send_and_await_response(f'pardon {player}', timeout=self._timeout)

    def set_whitelist(self, enable: bool) -> str | list[str] | None:
        if enable:
            return self.send_multiple_and_await_response(
                [
                    'whitelist on',
                    'kick @a[tag=!whitelisted] Server is now whitelisted due to bots.',
                ], timeout=self._timeout
            )
        else:
            return self.send_and_await_response('whitelist off', timeout=self._timeout)

    def get_scores(self, player: str, objectives: list[str]) -> dict[str, int | None]:
        '''Returns a dictionary of objective: value'''
        ret: dict[str, int | None] = dict()
        with self:
            for score in objectives:
                reply: str = self.send_and_await_response(message=f'scoreboard players get {player} {score}', timeout=self._timeout)
                if reply == "":
                    self.log.info(f'Score "{score}" request failed for player "{player}".')
                else:
                    reply_message = self.remove_log_prefix(str(reply))
                    regex = r'^(.*) has (-?[\d]+) \[([^\[\]]+)\]\n?$'
                    values = re.search(regex, reply_message)
                    if values:
                        plr, val, scr = values.groups()
                        if plr == player and scr == score:
                            ret.setdefault(score, int(val))
                            continue
                ret.setdefault(score, None)
        return ret
