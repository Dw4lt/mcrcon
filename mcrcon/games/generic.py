from .. import RconClientProtocol
import re
from ..components import Loggable, MessageTypes


class GenericRconClient(RconClientProtocol, Loggable):
    '''A more user-friendly wrapper around the RconClientProtocol implementation.

    Unless there is a very specific reason to be using the underlying class directly, use this one or one of its derivatives.'''

    def __init__(self, *, ip: str, password: str, port: int, timeout: float | None = None):
        super().__init__(ip=ip, password=password, port=port, timeout=timeout)

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, *args):
        self.disconnect()

    def _get_log_prefix_regex(self) -> str:
        raise NotImplementedError(
            'This method is not implemented. Make use of a derived class for application specific details.'
        )

    def send(
        self, message: str, message_type: int = MessageTypes.SERVERDATA_EXECCOMMAND
    ) -> int:
        '''Sends a message to the server and awaits the server response if requested.

        If a connection to the server is not already established, a temporarily one opened and then closed again afterwards.'''
        if self.is_session_valid():
            return self.__send_after_connection_established(message, message_type)
        else:
            with self:
                return self.__send_after_connection_established(message, message_type)

    def send_and_await_response(
        self, message: str, message_type: int = MessageTypes.SERVERDATA_EXECCOMMAND, timeout: float | None = None
    ) -> str:
        if self.is_session_valid():
            id = self.__send_after_connection_established(message, message_type)
            return self.await_response(id, timeout)
        else:
            with self:
                id = self.__send_after_connection_established(message, message_type)
                return self.await_response(id, timeout)

    def send_multiple(
        self, messages: list[str], message_type: int = MessageTypes.SERVERDATA_EXECCOMMAND
    ) -> list[int]:
        '''Sends multiple messages to the server and awaits the server responses if requested (input and output order is maintained).

        If a connection to the server is not already established, a temporarily one opened and then closed again afterwards.'''
        if self.is_session_valid():
            return [self.__send_after_connection_established(msg, message_type) for msg in messages]
        else:
            with self:
                return [self.__send_after_connection_established(msg, message_type) for msg in messages]

    def send_multiple_and_await_response(
        self, messages: list[str], message_type: int = MessageTypes.SERVERDATA_EXECCOMMAND, timeout: float | None = None
    ) -> list[str]:
        '''Sends multiple messages to the server and awaits the server responses if requested (input and output order is maintained).

        If a connection to the server is not already established, a temporarily one opened and then closed again afterwards.'''
        if self.is_session_valid():
            return [self.await_response(self.__send_after_connection_established(msg, message_type), timeout) for msg in messages]
        else:
            with self:
                return [self.await_response(self.__send_after_connection_established(msg, message_type), timeout) for msg in messages]

    def __send_after_connection_established(self, message: str, message_type: int) -> int:
        id = self._send(message_type=message_type, message=message)
        self.log.debug(f'Message sent: "{message}"')
        return id

    def remove_log_prefix(self, server_reply: str):
        '''Remove the prefix (log level, timestamp etc.) of a reply from the server.'''
        return re.sub(self._get_log_prefix_regex(), '', server_reply)

    def test_connection(self):
        '''Quickly connect and then disconnect from the server.

        Useful to debug connection issues.'''
        already_connected = self.is_session_valid()
        self._ensure_connected()
        self.log.info("Connection Established")
        if not already_connected:
            self.disconnect()
